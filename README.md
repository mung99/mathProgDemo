Dies ist ein Beispiel für ein Projekt in Mathematischer Programmierung

# Thema Kaffee-Maschine

## Team N.N ; M.M

## Funktionsbeschreibung
- Kann Kaffee kochen
- Verarbeitet ganze Bohnen
- Touch-Display für Bedienung
- Umfangreiche Hilfe

## Bezug zu Mathematik
Die Minimierung des Energieverbrauchs erfordert lineare Optimierung.

## Zeitplan
- Grobkonzept Ende Okt
- Feinkonzept Mitte Nov., Klärung benötigte Technologien
- Bis 2. Woche Dez.: Programmierung Grundgerüst 
- Prototyp für Test Ende Dez.

## Aufgabenteilung
- N. N. : interne Simulation
- M. M. : Benutzeroberfläche, Hilfesystem

## Zusammenarbeit
- Regelmäßige Treffen 14-tägig in Mensa
- Dokumente in gemeinsamer Dropbox
- Java-Code archiviert in code.google

## Dokumentation
- Basierend auf Javadoc
- Sprache Englisch


## Offene Punkte, Fragen
- Entwicklungsumgebung eclipse statt BlueJ verwenden?
- GUI mit Plotter oder direkt Java Swing
